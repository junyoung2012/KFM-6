kfm-6 项目纪要

1.开发目的

​	1.1 为了就业有项目

​	1.2 整合以前学习知识

​	1.3 收获项目的能力，开发流程，开发方式

​	1.4  记录在开发中遇到的困难和解决路径

2.开发目标

​	2.1 根据技术对应目标

​		2.1 前端

​			

​		2.2 后端

​			2.1 控制器框架 SpringBoot  

​			2.2 持久层技术 mybatis

​			2.3 容器管理框架 spring 

​			2.4 表示层 jsp



​			



​		2.3 数据库

​			2.3.1 MySQL 8.0.32

​	2.2 尽量让项目内容具有自己的特色



SpringBoot如何集成jsp？

使用弹性布局，完成一个主页面



3.项目开发过程

​	1.瀑布模型

​		1.1 项目定义阶段

​				1.1.1 可行性分析报 

​		1.2 需求分析阶段

​			1.2.1 需求分析说明书

​		1.3 设计阶段

​			1.3.1 概要设计

​			1.3.2 详细设计

​		1.4 编码阶段

​			1.4.1 实现代码

​			1.4.2 单元测试

​		1.5 项目的交付阶段

​				1.5.1 集成测试

​				1.5.2 交付测试

​	2.敏捷开发模型



4.定义需求（干什么）

​	快速原型法

5.Springboot项目的创建

​	5.1 Springboot项目的创建方式

​			5.1.1 idea创建

​			![image-20230523101311374](E:\KFM-6\kfm-6 项目纪要.assets\image-20230523101311374.png)

5.1.2 [Spring Initializr](https://start.spring.io/)  创建

5.1.3 spring boot cli创建应用   https://jingyan.baidu.com/article/f79b7cb330ebe79145023e54.html

![image-20230523103258631](E:\KFM-6\kfm-6 项目纪要.assets\image-20230523103258631.png)

6.Springboot 集成jsp

- 增加依赖

```XML
        <!-- 使用jsp引擎，springboot内置tomcat没有此依赖 -->
        <dependency>
            <groupId>org.apache.tomcat.embed</groupId>
            <artifactId>tomcat-embed-jasper</artifactId>
            <version>10.1.8</version>
        </dependency>
<!--        serlet支持-->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>3.0-alpha-1</version>
        </dependency>
<!--        jstl支持-->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>
```



- 配置

  - 视图解析器

  ```properties
  #jsp支持
  spring.mvc.view.prefix:/WEB-INF/jsp/
  spring.mvc.view.suffix:.jsp
  
  ```

  

- 添加webapp文件夹

  - src/main/webapp
  - ![image-20230523112651360](E:\KFM-6\kfm-6 项目纪要.assets\image-20230523112651360.png)

6.设计阶段

- 先类后表
  - Power desigin UML 类图
- **先表后类**
  - ER WIN 数据库建模工具

7.创建数据库

​	7.1 创建用户

``` sql
### 创建用户并指定密码
CREATE USER 'username'@'%' IDENTIFIED WITH mysql_native_password BY 'password';

GRANT ALL PRIVILEGES ON *.* TO u@'%' WITH GRANT OPTION;  -- 授权

FLUSH PRIVILEGES;
```



7.2 新用户登录



7.3 创建数据库

```sql
CREATE DATABASE DB_USER;
```

7.4 切换当前数据库

```sql
USE DB_USER;
```

7.5 创建表

```sql
-- drop TABLE T_User;
CREATE TABLE T_User(
  id INT AUTO_INCREMENT	 PRIMARY KEY,
  NAME	VARCHAR(10) NOT NULL,
  idCard CHAR(18) NOT NULL,
  mob CHAR(11)	NOT NULL UNIQUE
);
```



8.springboot集成mybatis

8.1 创建model

8.2 创建dao

​	8.2.1 封装BaseDao

8.3 添加依赖

```xml
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>3.0.2</version>
        </dependency>

        <dependency>
            <groupId>com.mysql</groupId>
            <artifactId>mysql-connector-j</artifactId>
            <scope>runtime</scope>
        </dependency>
```

8.4 配置

```properties
#数据源
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/DB_USER
spring.datasource.username=user_kfm
spring.datasource.password=123456

#mybatis
mybatis.type-aliases-package=com.kfm.kfmfirstdemo.model
mybatis.mapper-locations=classpath:mapper/*.xml
```



```java
@SpringBootApplication
@MapperScan(basePackages = "com.kfm.kfmfirstdemo.dao")
public class KfmFirstDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(KfmFirstDemoApplication.class, args);
    }

}
```



9.业务层测试

10.完成了控制层

​	10.1 请求路由

​	10.2 表示层

- 数据渲染（JSTL+EL）

  10.3 利用身份证，计算了 性别，年龄，所在省份

11.Echarts

​	11.1 静态的demo

- 引入类库
- 初始化echart对象
- 定义配置Option
- 绑定配置到echart对象

11.3 动态的demo

- ​	利用ajax获取数据
- fetch 获取远程、数据
- 在服务端产生远程数据
- 如何拓展功能，思路

11.4 使用了地图

- https://datav.aliyun.com/portal/school/atlas/area_selecto
- https://blog.csdn.net/SkelleBest/article/details/121204097

12 .拓展思路

- ​	批量导入用户数据
  - 上传文件到服务端
  - 在服务端可以解析excel文件 **easyPoi**



