package com.kfm.kfmfirstdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.kfm.kfmfirstdemo.dao")
public class KfmFirstDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(KfmFirstDemoApplication.class, args);
    }

}
