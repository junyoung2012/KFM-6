package com.kfm.kfmfirstdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
//@RestController
public class IndexController {
    @RequestMapping("/")//强制指定了欢迎页面
    public String index(){
        return "redirect:user/showData";//相对地址，不是视图名称
    }
    @RequestMapping(value = "hello")
    @ResponseBody
    public String hello(){
        return "hello1";//跳转的地址
    }
    @RequestMapping("test1")
    public String test1(){
//        /WEB-INF/jsp/ test01 .jsp
        return "test02";
    }
}
