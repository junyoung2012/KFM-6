package com.kfm.kfmfirstdemo.controller;

import com.kfm.kfmfirstdemo.model.ChartDataItem;
import com.kfm.kfmfirstdemo.model.User;
import com.kfm.kfmfirstdemo.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {
    @Resource
    private UserService userService;
    @RequestMapping(value = "add",method = RequestMethod.GET)
    public ModelAndView add(){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.setViewName("user/input");
        modelAndView.addObject("act","addSave");
        return modelAndView;
    }
    @RequestMapping(value = "addSave",method = RequestMethod.POST)
    public ModelAndView addSave(User user){
        if (userService.addUser(user)) {
            return list().addObject("message","保存成功");
        }
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.setViewName("user/input");
        modelAndView
                .addObject("act","addSave")
                .addObject("message","保存失败");
//                .addObject("user",user);
        return modelAndView;
    }
    @RequestMapping(value = "delete",method = RequestMethod.GET)
    public ModelAndView delete(User user){
//        userService.deleteUser(new User(id))
        String message=userService.deleteUser(user)?"删除成功":"删除失败";
        return list().addObject("message",message);
    }
//    打开修改页面
    @RequestMapping(value = "update",method = RequestMethod.GET)
    public ModelAndView update(int id){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.setViewName("user/input");
        User user = userService.getUserById(id);
        modelAndView
            .addObject("act","updateSave")
            .addObject("user",user);
        return modelAndView;
    }
//    修改保存
    @RequestMapping(value = "updateSave",method = RequestMethod.POST)
    public ModelAndView updateSave(User user){
        if (userService.updateUser(user)) {
            return list().addObject("message","保存成功");
        }else{
            ModelAndView modelAndView=new ModelAndView();
            modelAndView.setViewName("user/input");
            modelAndView
                .addObject("act","updateSave")
                .addObject("message","保存失败")
                .addObject("user",user);
            return modelAndView;
        }
    }
//    打开列表页
//    @RequestMapping(value = "list",method = RequestMethod.GET)
    @GetMapping("list")
    public ModelAndView list(){
        ModelAndView modelAndView=new ModelAndView("user/list");
        List<User> users = userService.getUsers();
        modelAndView.addObject("users",users);
        return modelAndView;
    }
    @GetMapping("showData")
    public ModelAndView showData(){
        ModelAndView modelAndView=new ModelAndView("user/showData2");
        return modelAndView;
    }
    @GetMapping("getTotalData")
    @ResponseBody
    public List<ChartDataItem> getTotalData(){
        List<ChartDataItem> chartDataItems=userService.getTotalData();
        return chartDataItems;
    }
}
