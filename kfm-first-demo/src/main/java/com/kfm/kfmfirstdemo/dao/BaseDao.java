package com.kfm.kfmfirstdemo.dao;


import java.util.List;

public interface BaseDao<T,ID> {
    boolean add(T obj);

    boolean delete(T obj);

    boolean update(T obj);

    T getById(ID id);

    List<T> getAll();
}
