package com.kfm.kfmfirstdemo.dao;

import com.kfm.kfmfirstdemo.model.ChartDataItem;
import com.kfm.kfmfirstdemo.model.User;
import org.springframework.web.bind.annotation.Mapping;

import java.util.List;
public interface UserDao extends BaseDao<User,Integer> {
    List<ChartDataItem> getTotalData();
}
