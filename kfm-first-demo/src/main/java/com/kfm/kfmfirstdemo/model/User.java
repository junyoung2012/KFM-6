package com.kfm.kfmfirstdemo.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class User {
    private int id;
    private String name;
    private String idCard;
    private String mob;
    private Provice provice;

    public User() {
    }

    public User(int id) {
        this.id = id;
    }

    public User(String name, String idCard, String mob) {
        this.name = name;
        this.idCard = idCard;
        this.mob = mob;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", idCard='" + idCard + '\'' +
                ", mob='" + mob + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
    public String getSex(){
        return (idCard.charAt(16)%2==1?"男":"女");
    }
    public double getAge(){
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyyMMdd");
        try {
            Date bitrthDay=dateFormat.parse(idCard.substring(6,14));
            long l = new Date().getTime() - bitrthDay.getTime();
            return l/1000/86400/365.25;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public Provice getProvice() {
        return provice;
    }

    public void setProvice(Provice provice) {
        this.provice = provice;
    }
}
