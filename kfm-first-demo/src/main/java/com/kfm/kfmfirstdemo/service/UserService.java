package com.kfm.kfmfirstdemo.service;

import com.kfm.kfmfirstdemo.model.ChartDataItem;
import com.kfm.kfmfirstdemo.model.User;

import java.util.List;

public interface UserService {
    public boolean addUser(User user);
    public boolean deleteUser(User user);
    public boolean updateUser(User user);
    public User getUserById(int id);
    public List<User> getUsers();

    List<ChartDataItem> getTotalData();
}
