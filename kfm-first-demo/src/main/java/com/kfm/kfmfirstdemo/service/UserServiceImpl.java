package com.kfm.kfmfirstdemo.service;

import com.kfm.kfmfirstdemo.dao.UserDao;
import com.kfm.kfmfirstdemo.model.ChartDataItem;
import com.kfm.kfmfirstdemo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class UserServiceImpl implements  UserService{
    @Resource
//    @Autowired
    private UserDao userDao;
    @Override
    public boolean addUser(User user) {
        try {
            return userDao.add(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteUser(User user) {
        return userDao.delete(user);
    }

    @Override
    public boolean updateUser(User user) {
        return userDao.update(user);
    }

    @Override
    public User getUserById(int id) {
        return userDao.getById(id);
    }

    @Override
    public List<User> getUsers() {
        return userDao.getAll();
    }

    @Override
    public List<ChartDataItem> getTotalData() {
        return userDao.getTotalData();
    }
}
