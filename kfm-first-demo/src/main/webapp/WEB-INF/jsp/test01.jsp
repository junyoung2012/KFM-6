<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2023/5/23
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>test01</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        div{
            border: 1px solid black;
        }
        .title{
            height: 100px;

        }
        .midele{
            /*height: calc(100% - 105px);*/
        }
        .nav{
            width: 200px;
            height: 100%;
            float: left;
            background: red;
        }
        .content{
            height: 100%;
            background: yellow;
        }
        .right{
            width: 200px;
            height: 100%;
            float: right;
            background: blue;
        }
    </style>
</head>
<body>
   <div class="title">

   </div>
    <div class="midele">
        <div class="nav">nav</div>
        <div class="right">right</div>
        <div class="content">content</div>
     </div>
</body>
</html>
