<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2023/5/23
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>test01</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        div{
            border: 1px solid black;
        }
        .wrap{
            display: flex;/* 弹性布局容器*/
            flex-direction: column;
            height: 100vh;
        }
        .title{
            flex-basis: 100px;
        }
        .midele{
            flex-grow: 1;
            /*background: blue;*/
            display: flex;/* 弹性布局容器*/
            flex-direction: row;
        }
        .nav{
            flex-basis: 200px;
        }
       .right{
            flex-basis: 200px;
        }
       .content{
           flex-grow: 1;
       }
    </style>
</head>
<body>
    <div class="wrap">
       <div class="title">
            aaaa
       </div>
        <div class="midele">
            <div class="nav">nav</div>
            <div class="content">content</div>
            <div class="right">right</div>
         </div>
    </div>
</body>
</html>
