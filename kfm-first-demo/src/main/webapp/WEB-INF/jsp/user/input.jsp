<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2023/5/23
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>用户信息统计系统</title>
    <base href="${pageContext.request.contextPath}/">
    <link href="style/public.css" rel="stylesheet">
    <style>
        /*@import "";*/
    </style>
</head>
<body>
<div class="wrap">
    <%@include file="../title.jsp" %>
    <div class="midele">
        <%@include file="../nav.jsp" %>
        <div class="content">
            <h2>${act=='addSave'?'用户录入':'修改用户'}</h2>
            <h3>${message}</h3>
            <form action="user/${act}" method="post">
                <table>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>姓名</td>
                        <td class="td-left">
                            <input type="hidden" name="id" value="${empty user?0:user.id}">
                            <input type="text" name="name" value="${user.name}">
                        </td>
                    </tr>
                    <tr>
                        <td>身份证号</td>
                        <td class="td-left"><input type="text" name="idCard" value="${user.idCard}"></td>
                    </tr>
                    <tr>
                        <td>联系手机</td>
                        <td class="td-left"><input type="text" name="mob" value="${user.mob}"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input class="btn" type="submit" value="保存">
                            <input class="btn" type="reset" value="取消">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
</body>
</html>
