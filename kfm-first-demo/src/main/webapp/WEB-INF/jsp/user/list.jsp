<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2023/5/23
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>用户信息统计系统</title>
    <base href="${pageContext.request.contextPath}/">
    <link href="style/public.css" rel="stylesheet">
    <style>
        /*@import "";*/
    </style>
</head>
<body>
<div class="wrap">
    <%@include file="../title.jsp" %>
    <div class="midele">
        <%@include file="../nav.jsp" %>
        <div class="content">
            <h2>用户录入</h2>
            <h3>${message}</h3>
            <table>
                <tr>
                    <th>编号</th>
                    <th>姓名</th>
                    <th>身份证号</th>
                    <th>联系手机</th>
                    <th>性别</th>
                    <th>年龄</th>
                    <th>省份</th>
                    <th>删除</th>
                    <th>修改</th>
                </tr>
                <c:forEach var="user" items="${users}">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.name}</td>
                        <td>${user.idCard}</td>
                        <td>${user.mob}</td>
                        <td>${user.sex}</td>
                        <td><fmt:formatNumber value="${user.age}" pattern="###.#" /></td>
                        <td>${user.provice.name}</td>
                        <td><a href="user/delete?id=${user.id}" onclick="return confirm('真的删除吗？')">删除</a></td>
                        <td><a href="user/update?id=${user.id}">修改</a></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
</body>
</html>
