<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2023/5/23
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>用户信息统计系统</title>
    <base href="${pageContext.request.contextPath}/">
    <link href="style/public.css" rel="stylesheet">
    <%--   1.CDN引入Echars类库--%>
    <script src="https://cdn.bootcdn.net/ajax/libs/echarts/5.4.2/echarts.js"></script>
</head>
<body>
<div class="wrap">
    <%@include file="../title.jsp" %>
    <div class="midele">
        <%@include file="../nav.jsp" %>
        <div id="main" class="content">
        </div>
    </div>
</div>
</body>
<script>
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));
    //定义配置
    var option = {
        title: {
            text: 'ECharts 入门示例'
        },
        tooltip: {},
        legend: {
            data: ['销量']
        },
        xAxis: {
            data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
        },
        yAxis: {},
        series: [
            {
                name: '销量',
                type: 'bar',
                data: [5, 20, 36, 10, 10, 50]
            }
        ]
    };
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);
</script>
</html>
