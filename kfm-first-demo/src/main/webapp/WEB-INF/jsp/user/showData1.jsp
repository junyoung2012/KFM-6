<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2023/5/23
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>用户信息统计系统</title>
    <base href="${pageContext.request.contextPath}/">
    <link href="style/public.css" rel="stylesheet">
    <%--   1.CDN引入Echars类库--%>
    <script src="https://cdn.bootcdn.net/ajax/libs/echarts/5.4.2/echarts.js"></script>
</head>
<body>
<div class="wrap">
    <%@include file="../title.jsp" %>
    <div class="midele">
        <%@include file="../nav.jsp" %>
        <div id="main" class="content">
        </div>
    </div>
</div>
</body>
<script>
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));
    //定义配置
    var option = {
        title: {
            text: '客户分析',
            subtext: '客户所在省份分析',
            left: 'center'
        },
        tooltip: {
            trigger: 'item'
        },
        legend: {
            orient: 'vertical',
            left: 'left'
        },
        series: [
            {
                name: 'Access From',
                type: 'pie',
                radius: '50%',
                data: [
                    { value: 1048, name: 'Search Engine' },
                    { value: 735, name: 'Direct' },
                    { value: 580, name: 'Email' },
                    { value: 484, name: 'Union Ads' },
                    { value: 300, name: 'Video Ads' }
                ],
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };


    fetch("user/getTotalData").then((ret)=>{
        // console.log(ret.json());
        return  ret.json();
    }).then((json)=>{
        console.log(json);
        option.series[0].data=json;
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    }).catch((err)=>{
        console.log(err);
    })
</script>
</html>
