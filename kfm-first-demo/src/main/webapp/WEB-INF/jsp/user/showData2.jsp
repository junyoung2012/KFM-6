<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2023/5/23
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>用户信息统计系统</title>
    <base href="${pageContext.request.contextPath}/">
    <link href="style/public.css" rel="stylesheet">
    <%--   1.CDN引入Echars类库--%>
    <script src="https://cdn.bootcdn.net/ajax/libs/echarts/5.4.2/echarts.js"></script>
    <script src="china.js" ></script>
</head>
<body>
<div class="wrap">
    <%@include file="../title.jsp" %>
    <div class="midele">
        <%@include file="../nav.jsp" %>
        <div id="main" class="content">
        </div>
    </div>
</div>
</body>
<script>
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));
    //定义配置
    let option = {
        backgroundColor: "#E8EFFE",
        title: {
            text: '客户分析',
            subtext: '客户所在省份分析',
            left: 'center'
        },
        geo: {
            type: 'map',
            map: 'china', //chinaMap需要和registerMap中的第一个参数保持一致
            roam: true, // 设置允许缩放以及拖动的效果
            label: {
                show: true  //展示标签
            },
            zoom: 1, //设置初始化的缩放比例
        },

        series: [
            {
                // data: airData,
                geoIndex: 0,  //将空气质量的数据和第0个geo配置关联在一起
                type: 'map',
            }
        ],
        visualMap: {
            min: 0,
            max: 5,
            inRange: {
                color: ['white', '#5475f5'], //控制颜色渐变的范围
            },
            calculable: true //出现滑块
        },
        tooltip: {
            triggerOn: "mousemove",   //mousemove、click
            padding: 8,
            borderWidth: 1,
            borderColor: '#409eff',
            backgroundColor: 'rgba(255,255,255,0.7)',
            textStyle: {
                color: '#000000',
                fontSize: 13
            },
            formatter: function (e, t, n) {
                let data = e.data;
                console.log(data)
                if(e.data==null) return "暂无数据";
                let context = `
                            <div>
                                <p><b style="font-size:15px;">\${data.name}</b></p>
                                <p class="tooltip_style"><span class="tooltip_left">总数</span><span class="tooltip_right">\${data.value}</span></p>
                            </div>
                            `
                return context;
            }
        },
    }
    // 地图注册，第一个参数的名字必须和option.geo.map一致
    echarts.registerMap("china", zhongguo);


    fetch("user/getTotalData").then((ret)=>{
        // console.log(ret.json());
        return  ret.json();
    }).then((json)=>{
        console.log(json);
        option.series[0].data=json;
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    }).catch((err)=>{
        console.log(err);
    })
</script>
</html>
