package com.kfm.kfmfirstdemo.service;

import com.kfm.kfmfirstdemo.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class UserServiceTest {
    @Resource
    private UserService userService;
    @Test
    void addUser() {
        User user=new User("张三","610101200203050018","13100010001");
        Assert.isTrue( userService.addUser(user));
    }

    @Test
    void deleteUser() {
    }

    @Test
    void updateUser() {
    }

    @Test
    void getUserById() {
    }

    @Test
    void getUsers() {
        List<User> users = userService.getUsers();
        System.out.println(users);
    }
}